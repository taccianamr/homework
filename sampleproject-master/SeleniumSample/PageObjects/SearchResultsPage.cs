﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace SeleniumSample.PageObjects
{
    public class SearchResultsPage
    {
        private IWebDriver driver;
        private readonly By searhResultKeywordLocator = By.XPath("//div[@class=\'b-search-note\']/b");
        public SearchResultsPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public string ResultKeyword => driver.FindElement(searhResultKeywordLocator).Text;
    }
}
